import { defineStore } from 'pinia';

export const useDogStore = defineStore('dog', {
    state: () => ({
        imageHome: [],
        listAllDog: [],
        mainDogPhotos: [],
        subDogPhotos: [],
        subBreedList: null
      }),
    getters: {
        mainBreeds: (state) => Object.keys(state.listAllDog)
    },
    actions: {
        async getImageHome() {
          const { data } = await useFetch('https://dog.ceo/api/breeds/image/random/3');
          if (data.value) {
            this.imageHome = data.value.message;
          }
        },
        async getListDog() {
          const { data } = await useFetch('https://dog.ceo/api/breeds/list/all');
          if (data.value) {
            this.listAllDog = data.value.message;
          }
        },
        async getMainDogPhotos(name) {
          const { data } = await useFetch(`https://dog.ceo/api/breed/${name}/images/random/4`);
          if (data.value) {
            this.mainDogPhotos = data.value.message;
          }
        },
        async getSubDogPhotos(mainDog,subDog) {
          const { data } = await useFetch(`https://dog.ceo/api/breed/${mainDog}/${subDog}/images/random/4`);
          if (data.value) {
            this.subDogPhotos = data.value.message;
          }
        },
        async getSubBreedList(mainDog) {
          const { data } = await useFetch(`https://dog.ceo/api/breed/${mainDog}/list`);
          if (data.value.message) {
            this.subBreedList = data.value.message;
          }
        },
    },
});