// https://nuxt.com/docs/api/configuration/nuxt-config
export default defineNuxtConfig({
  modules: [
    '@bootstrap-vue-next/nuxt',
    '@pinia/nuxt',
  ],
  css: ['bootstrap/dist/css/bootstrap.min.css'],
  imports: {
    dirs: ['./store'],
  },

  pinia: {
    autoImports: ['defineStore', 'definePiniaStore'],
  }
})
